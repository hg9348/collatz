#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C)
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------

cache = {1: 1}


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = list(map(int, s.split())
             )       # creates an integer list that stores the two ends of the interval
    return a                            # returns the interval for evaluation

# ------------
# collatz_eval
# ------------


def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    lst = []                           # creates a list that stores cycle lengths

    if i > j:                          # sort the list if the first number is greater than the second
        a, b = j, i                    # protects the original data by creating two new variables
    else:
        a, b = i, j

    for n in range(a, b + 1):          # iterates through the interval to find the max cycle length
        if n in cache:
            # if the number is already a key, returns its value
            lst.append(cache[n])
        else:
            # else runs the recursive function to find its cycle length
            lst.append(collatz_eval_helper(n))

    return max(lst)                    # returns the max cycle length

# -------------------
# collatz_eval_helper
# -------------------


def collatz_eval_helper(n):
    """
    n the current evaluating number
    check the presence of n in cache
    return the key value if present
    continue recursion if not
    """
    if n in cache:
        # if the number is already a key, returns its value
        return cache[n]
    else:
        if n % 2 == 0:                 # if the number is even, divides it by two
            cache[n] = 1 + collatz_eval_helper(n//2)
            return cache[n]
        else:                          # if the number is odd, multiplies it by three and add one to it
            cache[n] = 1 + collatz_eval_helper(3 * n + 1)
            return cache[n]

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) +
            "\n")        # prints the result like "1 10 20"

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        # reads the input
        i, j = collatz_read(s)
        # finds the max cycle length
        v = collatz_eval(i, j)
        # prints the output
        collatz_print(w, i, j, v)
